import Vue from 'vue';
import Vuex from 'vuex';


import axios from 'axios';
// import lodash from 'lodash';

Vue.use(Vuex);

const state = {
    url: 'http://localhost:3000/employees',
    employeeLst: [],
    totalCount: 0,
    numberPerPage: 10,
    currentPageCount: 10,
    startPage: 1,
    query: '',
    isActiveList: true,
    cities: ['Bath', 'London', 'Paris', 'Berlin', 'Malaga', 'Hintzmouth'],
    countries: [
        'United Kingdom', 'France', 'Germany', 'Spain', 'Netherlands',
        'Greece', 'Denmark', 'Croatia', 'Argentina', 'Gibraltar', 'Thailand',
        'Portugal', 'Holy See (Vatican City State)', 'United States of America', 'Brazil', 'Uganda', 'Taiwan'

    ]
};

const mutations = {
    SET_EMPLOYEE_LIST: (state, list) => {
        state.employeeLst = list;
    },

    SET_TOTAL_COUNT: (state, count) => {
        state.totalCount = count;
    },

    SET_START_PAGE: (state, newStartPage) => {
        state.startPage = newStartPage;
    },

    SET_QUERY: (state, query) => {
        state.query = query;
    },
    SET_ACTIVE_LIST: (state, isActiveList) => {
        state.isActiveList = isActiveList;
    },
    SET_CURRENT_PAGE_COUNT: (state, currentPageNo) => {
        state.currentPageCount = currentPageNo;
    }

};


const actions = {
    fetchEmployee: function({commit}) {
        axios.get(state.url+'?_page='+state.startPage+'&_limit='+state.numberPerPage+'&_sort=name&active='+state.isActiveList+'&q='+state.query)
        .then(function(response){
            commit('SET_EMPLOYEE_LIST', response.data);
            commit('SET_CURRENT_PAGE_COUNT', response.data.length);
        });
    },

    countOrignalTotal: function({commit}) {
        axios.get(state.url+'?active='+state.isActiveList+'&q='+state.query)
        .then(function(response){
            var data = response.data,
                totalCount = data.length;

            commit('SET_TOTAL_COUNT', totalCount);
        });
    },

    updateUser: function({commit, dispatch}, employee) {
        axios({
            method: 'put',
            url: state.url+'/'+employee.id,
            data: {
                name: employee.name,
                job: employee.job,
                jobArea: employee.jobArea,
                avatar: employee.avatar,
                phone: employee.phone,
                email: employee.email,
                address: employee.address,
                active: !employee.active
            },
            headers: {'Content-Type': 'application/json'}
        }).then(function(response){

            var totalCount = state.totalCount, //get the total users
                currentPageCount = state.currentPageCount,
                startPage = state.startPage;

            totalCount--;//minus one because they are now minus 1 active or inactive
            currentPageCount--;

            if( currentPageCount == 0 ) {
                startPage--;
                if( startPage == 0 ) startPage = 1;
            }

            commit('SET_START_PAGE', startPage);
            dispatch('fetchEmployee');
            commit('SET_TOTAL_COUNT', totalCount);


        }).catch(function(error){
            console.log(error);
        });
    },

    editUser: function({commit}, data) {
        var employee = data.employee,
            isActive = false,
            method = 'post',
            avatar = 'http://placebeard.it/g/128/',
            params = {
                name:    employee.name,
                job:     employee.job,
                jobArea: employee.jobArea,
                phone:   employee.phone,
                email:   employee.email,
                address: employee.address,
            };

        if( data.action == 'edit' ) {
            isActive = employee.active;
            avatar = employee.avatar;
            method = 'put';
        }else {
            params.id = 'ID7567'+  Date.now();
        }

        params.avatar = avatar;
        params.active = isActive;

        axios({
            method: method,
            url: data.url,
            data: params,
            headers: {'Content-Type': 'application/json'}
        }).then(function(response){
            commit('SET_START_PAGE', 1);
        }).catch(function (err) {
            console.log(err);
        });
    }
};



const getters = {};


export const store = new Vuex.Store({
    state,
    mutations,
    actions,
    getters
})
