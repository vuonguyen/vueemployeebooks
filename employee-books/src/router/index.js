import Vue from 'vue'
import Router from 'vue-router'
import EmployeeList from '@/components/EmployeeList'
import SingleEmployee from '@/components/SingleEmployee'
import EmployeeForm from '@/components/EmployeeForm'

Vue.use(Router)

export default new Router({
    mode: 'history',
  routes: [
    {
      path: '/',
      name: 'list',
      component: EmployeeList
    },

    {
        path:'/profile/:id',
        name: 'single',
        component: SingleEmployee
    },

    {
        path:'/edit/:id',
        name: 'edit',
        component: EmployeeForm,
        props: {title: 'Edit employee', action: 'edit'}
    },


    {
        path: '/add',
        name: 'add',
        component: EmployeeForm,
        props: {title: 'Add new employee', action: 'add'}
    }
  ]
})
