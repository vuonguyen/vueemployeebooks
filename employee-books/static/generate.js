module.exports = function(){
   var faker = require('faker');
   var _ = require('lodash');
   var bcrypt = require('bcrypt');
   var saltRounds = 3;
   return {
       employees: _.times(50, function(n){
           var t = "ID" + n;
           var salt = bcrypt.genSaltSync(saltRounds);
           var id = bcrypt.hashSync(t, salt);
           return {
               id: id,
               name: faker.name.findName(),
               job: faker.name.jobTitle(),
               jobArea: faker.name.jobArea(),
               avatar: faker.internet.avatar(),
               phone: faker.phone.phoneNumber(),
               email: faker.internet.email(),
               address: {
                   street: faker.address.streetAddress(),
                   city: faker.address.city(),
                   country: faker.address.country()
               },
               active: faker.boolean

           }
       })
   }
}
